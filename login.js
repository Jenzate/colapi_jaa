// LOGIN - LOGOUT con repositorio mongo accedido a traves de Mlbab

var express = require('express');
var bodyParser = require('body-parser');//verificar si llega un json en el body
var requestJson = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./userlog.json');
var URLbase = "/colapi/V3/";
var baseMLabURL = "https://api.mlab.com/api/1/databases/colapidb_jaa/collections/";
var apiKey = "apiKey=18ZS4o9NEsF82rr5Y--4CeRRbtkqnfE7";
var queryString = 'f={"_id":0}&';

app.listen(port);
app.use(bodyParser.json());         // bodyParser libreria node_modules

console.log("COLAPI v3, login- logout escuchando por el puerto"+port+ "...");


//---------------------- Modulo Usuarios------------------------------------

// GET generica recupera todos los usuarios
// URL localhost:3000/colapi/V3/users/

app.get(URLbase + 'users/',
function(req, res) {
  console.log("clientes HTTP mLab recuperados");
  var httpClient = requestJson.createClient(baseMLabURL); // instancia la clase requestjson y a traves del metodo createClient realiza la consulta a bd mongp usando Mlab
  httpClient.get('user?'+queryString+ apiKey,
    function(err, respuestaMlab, body){
      console.log("err: "+err);

      var respuesta ={};
      respuesta = !err ? body: {"msg" : "Error al recuperar user de mLab"}
      res.send(respuesta)
  });

});


// ------- LOGIN de un usuario enviado por parametro desde el front ----
// URL localhost:3000/colapi/V3/login/

app.post(URLbase + 'login/',
 function(req, res) {
   console.log("GET /Colapi/v3/login/:id");
   console.log("desde front"+req.body);
   var email = req.body.email;
   var password = req.body.password;
   var httpClient= requestJson.createClient(baseMLabURL);
   var queryStringid = 'q={"email":'+'"'+email+'"'+','+'"password":'+'"'+password+'"'+'}&';

   console.log(" query: "+ queryStringid);

   // obtener los datos de mLab del usuario enviado desde el front
   httpClient.get('user?'+queryString +queryStringid + apiKey,
    function(err, respuestaMlab, body){
      console.log("respuesta Mlab para get correcta.. err: "+err+" respuesta: " +respuestaMlab);
      respuesta = body[0];

      if (body.length > 0){
        console.log("longitud: "+body.length);
        console.log("body.password: "+ body[0].password);

          clienteMlab = requestJson.createClient(baseMLabURL + "/user");
          var log = {"logged":true};
          var cambio = '{"$set":' + JSON.stringify(log) + '}'

          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
          function(err, resM, body) {
            res.send({"msg":"usuario logueado"});
          });
        }else{send({"msg":"usuario no logueado"});}
        //  }
    });
});

// ------- LOGOUT de un usuario enviado por parametro desde el front ----
// URL localhost:3000/colapi/V3/logout/

app.post(URLbase + 'logout/',
 function(req, res) {
   console.log("GET /Colapi/v3/logout/");
   console.log("desde front"+req.body);
   var email = req.body.email;
   var password = req.body.password;
   var httpClient= requestJson.createClient(baseMLabURL);
   var queryStringid = 'q={"email":'+'"'+email+'"'+','+'"password":'+'"'+password+'"'+'}&';

   console.log(" query: "+ queryStringid);

   // obtener los datos de mLab del usuario enviado desde el front
   httpClient.get('user?'+queryString +queryStringid + apiKey,
    function(err, respuestaMlab, body){
      console.log("respuesta Mlab para get correcta.. err: "+err+" respuesta: " +respuestaMlab+" Body:"+ body[0]);
      respuesta = body[0];

      if (body.length > 0){
        console.log("longitud: "+body.length);
        console.log("body.password: "+ body[0].password);

          clienteMlab = requestJson.createClient(baseMLabURL + "/user");
          var log = {"logged":true};                                // asignar la propiedad al usuario
          var cambio = '{"$unset":' + JSON.stringify(log) + '}'     // pasarla a string

          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), //agregar la propiedad al usuario
          function(err, resM, body) {
            res.send({"msg":"sesion finalizada", "mensaje mLab":body});
          });
        }else{send({"msg":"No se pudo cerrar la conexion"});}
        //  }
    });
});
